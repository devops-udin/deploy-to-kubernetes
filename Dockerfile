FROM node:lts-buster
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 9090
CMD ["node", "app.js"]