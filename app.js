const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');

var homepage = {
  title: "Pengajian Rutin Plupuh 5",
  date: (new Date()).toLocaleDateString(),
  type: "landingpage",
  ayat_title: "Al Fatihah Surah ke 1 ayat 1",
  ayat_prase: "بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِ",
  ayat_translate: "Dengan nama Allah Yang Maha Pengasih, Maha Penyayang.",
  background: "public/images/Landing.png"
}

const port = process.env.PORT || 9090;
const index = fs.readFileSync("index.html");
const admin = fs.readFileSync("admin.html")

function staticFile(req, res) {
  const parsedUrl = url.parse(req.url);
  // extract URL path
  let pathname = `.${parsedUrl.pathname}`;
  // based on the URL path, extract the file extension. e.g. .js, .doc, ...
  const ext = path.parse(pathname).ext;
  // maps file extension to MIME typere
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword'
  };

  fs.exists(pathname, function (exist) {
    if(!exist) {
      // if the file is not found, return 404
      res.statusCode = 404;
      res.end(`File ${pathname} not found!`);
      return;
    }

    // if is a directory search for index file matching the extension
    if (fs.statSync(pathname).isDirectory()) pathname += '/index' + ext;

    // read file from file system
    fs.readFile(pathname, function(err, data){
      if(err){
        res.statusCode = 500;
        res.end(`Error getting the file: ${err}.`);
      } else {
        // if the file is found, set Content-type and send data
        res.writeHead(200, {'Content-type' : map[ext] || 'text/plain'} );
        res.end(data);
      }
    });
  });
}

function updateData(data) {
  if (data.type == "ayat") {
    if (data.from == "quran") {
      const dataSurah = JSON.parse(fs.readFileSync('database/surah/' + data.surah + '.json', 'utf8'))[data.surah];
      homepage.ayat_title = dataSurah.name_latin + " Surah ke " + data.surah + " Ayat ke " + data.ayat
      homepage.ayat_prase = dataSurah.text[data.ayat]
      homepage.ayat_translate = dataSurah.translations.id.text[data.ayat]
    } else {
      homepage.ayat_title = data.title_custom
      homepage.ayat_prase = data.ayat_custom
      homepage.ayat_translate = data.translate_custom
    }
  }

  homepage.type = data.type
  homepage.background = data.background
}

const server = http.createServer((req, res) => {
  // Server-sent events endpoint
  if (req.url === '/events') {
    res.writeHead(200, {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache',
      'Connection': 'keep-alive',
    });

    const refreshRate = 1000; // in milliseconds
    return setInterval(() => {
      const id = Date.now();
      const data = JSON.stringify(homepage)
      const message =`retry: ${refreshRate}\nid:${id}\ndata: ${data}\n\n`;
      res.write(message);
    }, refreshRate);
  }

  const regex = new RegExp('^/public');
  if (regex.test(req.url)) {
    staticFile(req, res)
  }

  if (req.url === "/admin") {
    homepage.background = "public/images/Background1.png"
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(admin)
  }

  if (req.url === "/update") {
    if (req.method === "POST") {
      res.writeHead(200, {'Content-Type': 'text/html'});
      let body = '';
      req.on('data', chunk => {
        body += chunk.toString();
      });
      req.on('end', () => {
        updateData(JSON.parse(body))
        res.end('ok');
      });
      res.end("Success")
    } else {
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end("Invalid Method")
    }
  }

  // Client side
  res.writeHead(200, {'Content-Type': 'text/html'});
  setInterval(() => res.end(index), 1000)
});

server.listen(port);

server.on('error', (err) => {
  console.log(err);
  process.exit(1);
});

server.on('listening', () => {
  console.log(`Listening on port ${port}`);
});